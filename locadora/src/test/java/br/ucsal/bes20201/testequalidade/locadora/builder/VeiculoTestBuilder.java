package br.ucsal.bes20201.testequalidade.locadora.builder;

import br.ucsal.bes20201.testequalidade.locadora.dominio.Modelo;
import br.ucsal.bes20201.testequalidade.locadora.dominio.Veiculo;
import br.ucsal.bes20201.testequalidade.locadora.dominio.enums.SituacaoVeiculoEnum;

public class VeiculoTestBuilder {
	
	private static final String DEFAULT_PLACA = "UCS4L20";
	private static final Integer DEFAULT_ANO = 2012;
	private static final Modelo DEFAULT_MODELO = new Modelo("Renault Sandero");
	private static final Double DEFAULT_VALOR_DIARIA = 50.00;
	private static final SituacaoVeiculoEnum DEFAULT_SITUACAO = SituacaoVeiculoEnum.DISPONIVEL;
	
	private String placa = DEFAULT_PLACA;
	private Integer ano = DEFAULT_ANO;
	private Modelo modelo = DEFAULT_MODELO;
	private Double valorDiaria = DEFAULT_VALOR_DIARIA;
	private SituacaoVeiculoEnum situacao = DEFAULT_SITUACAO;
	
	private VeiculoTestBuilder() {}
	
	public static VeiculoTestBuilder aCarro() {
		return new VeiculoTestBuilder();
	}
	
	public VeiculoTestBuilder withPlaca(String placa) {
		this.placa = placa;
		return this;
	}
	
	public VeiculoTestBuilder fromAno(Integer ano) {
		this.ano = ano;
		return this;
	}
	
	public VeiculoTestBuilder fromModelo(Modelo modelo) {
		this.modelo = modelo;
		return this;
	}

	public VeiculoTestBuilder withValorDiaria(Double valorDiaria) {
		this.valorDiaria = valorDiaria;
		return this;
	}

	public VeiculoTestBuilder withSituacao(SituacaoVeiculoEnum situacao) {
		this.situacao = situacao;
		return this;
	}
	
	public VeiculoTestBuilder but() {
		return aCarro().withPlaca(placa).fromAno(ano).fromModelo(modelo).withValorDiaria(valorDiaria).withSituacao(situacao);
	}
	
	public Veiculo build() {
		Veiculo veiculo = new Veiculo();
		veiculo.setPlaca(placa);
		veiculo.setAno(ano);
		veiculo.setModelo(modelo);
		veiculo.setValorDiaria(valorDiaria);
		veiculo.setSituacao(situacao);
		return veiculo;		
	}
	
}

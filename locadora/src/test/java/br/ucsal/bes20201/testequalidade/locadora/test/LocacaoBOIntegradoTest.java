package br.ucsal.bes20201.testequalidade.locadora.test;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import br.ucsal.bes20201.testequalidade.locadora.builder.VeiculoTestBuilder;
import br.ucsal.bes20201.testequalidade.locadora.business.LocacaoBO;
import br.ucsal.bes20201.testequalidade.locadora.dominio.Modelo;
import br.ucsal.bes20201.testequalidade.locadora.dominio.Veiculo;

public class LocacaoBOIntegradoTest {

	/**
	 * Testar o cálculo do valor de locação por 3 dias para 2 veículos com 1 ano de
	 * fabricaçao e 3 veículos com 8 anos de fabricação.
	 */
	
	
	/**
	 * 	Veículo  | 		Valores de entrada		| 	Saída esperada	| 		Observações							|
	 * ----------------------------------------------------------------------------------------------------------
	 *		1    |		Valor=50.00 / Ano=2012	| 		135,00		|	*10% de desconto aplicado em cima do total
	 *		2	 |		Valor=65.65 / Ano=2012	|		177,255		|	*10% de desconto aplicado em cima do total
	 *		3	 |		Valor=40,48 / Ano=2012	|		109,30		|	*10% de desconto aplicado em cima do total
	 *		4	 |		Valor=75.43 / Ano=2019	|		226,296		| 	Veículo sem desconto
	 *		5	 |		Valor=91.00 / Ano=2019	|		273,00		|	Veículo sem desconto
	 *------------------------------------------------------------------------------------------------------------
	 * 						SAÍDA ESPERADA TOTAL = 		920,841
	 */
	
	@Test
	public void testarCalculoValorTotalLocacao5Veiculos3Dias() {
		
		VeiculoTestBuilder veiculoTestBuilder = VeiculoTestBuilder.aCarro();
		
		Veiculo carro1 = veiculoTestBuilder.build(); //Utilizando as informações DEFAULT para montar o primeiro carro
		Veiculo carro2 = veiculoTestBuilder.but().fromModelo(new Modelo("Volkswagen Gol")).withValorDiaria(65.65).build();  //Aproveitando o ano padrão de 2012
		Veiculo carro3 = veiculoTestBuilder.but().fromModelo(new Modelo("Chevrolet Celta")).withValorDiaria(40.48).build(); //Aproveitando o ano padrão de 2012
		Veiculo carro4 = veiculoTestBuilder.but().fromModelo(new Modelo("Honda Civic")).fromAno(2019).withValorDiaria(75.43).build(); //Alterando o ano para 2019
		Veiculo carro5 = veiculoTestBuilder.but().fromModelo(new Modelo("Toyota Corolla")).fromAno(2019).withValorDiaria(91.00).build(); //Alterando o ano para 2019
		
		//Criando lista de carros para o teste
		
		List<Veiculo> veiculos = new ArrayList<Veiculo>();
		veiculos.add(carro1);
		veiculos.add(carro2);
		veiculos.add(carro3);
		veiculos.add(carro4);
		veiculos.add(carro5);
		
		Double expectedResult = 920.841;
		Double actualResult = LocacaoBO.calcularValorTotalLocacao(veiculos, 3);
		
		Assertions.assertEquals(expectedResult, actualResult);
		
	}

}
